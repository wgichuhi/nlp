﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Samples
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("enter a phrase");
                string s = Console.ReadLine();
                List<string> ngrams = NGramGenerator.Generate(2, s);
                float prodProbInbox = 1F, prodProbSpam = 1F;
                EmailTitles titles = FileParser.ReadFile();

               
                //inbox titles                              
                for (int x = 0; x < ngrams.Count;  x++)
                {
                    string gram = ngrams[x];
                    int globalCount = 0, leftCount = 0;
                    float currentGramInboxProb = 0F, currentGramSpamProb = 0F;
                    foreach (var title in titles.Inbox)
                    {
                        if (RegexUtil.TryMatch(gram, title))
                            globalCount++;

                        if (RegexUtil.TryMatch(gram.Split()[0], title))
                            leftCount++;
                    }
                    currentGramInboxProb += (float)globalCount / (float)leftCount;
                    globalCount = 0;
                    leftCount = 0;

                    foreach (var title in titles.Spam)
                    {
                        if (RegexUtil.TryMatch(gram, title))
                            globalCount++;

                        if (RegexUtil.TryMatch(gram.Split()[0], title))
                            leftCount++;
                    }
                    currentGramSpamProb += (float)globalCount / (float)leftCount;

                    prodProbInbox *= currentGramInboxProb;
                    prodProbSpam *= currentGramSpamProb;
                }

                Console.WriteLine("inbox {0:0.00}", prodProbInbox);
                Console.WriteLine("spam {0:0.00}", prodProbSpam);

                Console.ReadLine();
            }
        }
    }
}
