﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace Samples
{
    public class RegexUtil
    {
        public static bool TryMatch(string pattern, string input)
        {
            Match match = Regex.Match(input, pattern);

            if (match.Success)
                return true;
            else
                return false;
        }
    }
}
